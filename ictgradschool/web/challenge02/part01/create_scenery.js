window.addEventListener("load", function(){

    let sizeAdjust = 1;
    let horizontalAdjust = 0;
    let verticalAdjust = 0;

    document.querySelectorAll("#choose-background [type=radio]").forEach(function (element) {

        // set values to default positions; needed in case the user refreshes the page
        if (element.id === "radio-background1") { element.checked = true; }

        element.addEventListener("change", function (event) {
            const radioId = String(event.target.id);
            const splitId = radioId.split("-");
            const backgroundName = splitId[splitId.length - 1];
            document.querySelector("#background").setAttribute("src", getBackgroundImage(backgroundName));
        });
    });

    document.querySelectorAll("#toggle-components [type=checkbox]").forEach(function (element) {

        // set values to default positions; needed in case the user refreshes the page
        element.checked = true;

        element.addEventListener("change", function (event) {
            const checkboxId = String(event.target.id);
            const splitCheckboxId = checkboxId.split("-");
            const imageId = splitCheckboxId[splitCheckboxId.length - 1];
            toggleVisibility(`#${imageId}`);
        });
    });

    document.querySelectorAll("#adjust-components [type=range]").forEach(function (element) {

        // set values to default positions; needed in case the user refreshes the page
        element.value = 0;

        element.addEventListener("change", function (event) {
            saveValue(event.target);
            transformDolphins();
        });
    });

    function getBackgroundImage(imageName) {
        if (imageName.charAt(imageName.length - 1) === "6") {
            return `../images/dolphins/${imageName}.gif`;
        } else {
            return `../images/dolphins/${imageName}.jpg`;
        }
    }

    function toggleVisibility(elementId) {
        const element = document.querySelector(elementId);
        if (element.getAttribute("hidden") === "true") {
            element.removeAttribute("hidden")
        } else {
            element.setAttribute("hidden", "true");
        }
    }

    function saveValue(formElement) {
        switch (formElement.id) {
            case "vertical-control":
                verticalAdjust = formElement.value;
                break;
            case "horizontal-control":
                horizontalAdjust = formElement.value;
                break;
            case "size-control":
                sizeAdjust = 1 + formElement.value / 100;
                break;
        }
    }

    function transformDolphins() {
        document.querySelectorAll("img.dolphin").forEach(function (element) {
            element.setAttribute("style", `transform: translate(${verticalAdjust}px, ${horizontalAdjust}px) scale(${sizeAdjust});`)
        });
    }
});