window.addEventListener("load", function(){

    const pageForwardClass = "flipped";
    const pageBackwardClass = "unflipped";
    const lastPage = "page11";

    document.querySelectorAll(".page").forEach(function (element) {
        if (element.id === lastPage) { return; } //respecting the comment (now deleted) from the CSS that the final cover shouldn't be turned

        element.addEventListener("click", function(event) {
            if (hasClass(event.target, pageForwardClass)) {
                event.target.classList.replace(pageForwardClass, pageBackwardClass);
            } else {
                event.target.classList.add(pageForwardClass);
            }
        });

        element.addEventListener("animationend", function (event) {

            let isBackwards = false;
            if (hasClass(event.target, pageBackwardClass)) {
                event.target.classList.remove(pageBackwardClass);   //if the reader has gone backwards a page, remove the class to prevent z-index issues
                isBackwards = true;
            }

            if (event.target.id === lastPage) {
                return;
            }

            const nextSibling = document.querySelector(`#${event.target.id}+.page`);
            if (isBackwards) {
                nextSibling.removeAttribute("style");   //reset to CSS defined z-index after back step to prevent z-index issues
            } else {
                nextSibling.style.zIndex = "0";
            }
        });
    });

    function hasClass(element, className) {
        let result = false;
        element.classList.forEach(function (classElement) {
            if (classElement === className) {
                result = true;
            }
        });
        return result;
    }

});